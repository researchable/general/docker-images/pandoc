## [1.1.1](https://gitlab.com/researchable/general/docker-images/pandoc/compare/v1.1.0...v1.1.1) (2021-06-03)


### Bug Fixes

* update versions ([27e4b34](https://gitlab.com/researchable/general/docker-images/pandoc/commit/27e4b34905a73c317f6a5edba65184efac99d3ee))

# [1.1.0](https://gitlab.com/researchable/general/docker-images/pandoc/compare/v1.0.0...v1.1.0) (2021-05-24)


### Bug Fixes

* remove .releaserc ([b4b956a](https://gitlab.com/researchable/general/docker-images/pandoc/commit/b4b956a3ef567e57e1bc3477908fa5e4678c0743))


### Features

* merged ([19c3ade](https://gitlab.com/researchable/general/docker-images/pandoc/commit/19c3ade5ca5a5a64c485b0176de1a72a4aab5314))
* update pandoc ([fe3f304](https://gitlab.com/researchable/general/docker-images/pandoc/commit/fe3f30493c3c9ab347893421e6ce436f685c4133))

# 1.0.0 (2021-04-15)


### Bug Fixes

* initial import ([be8704d](https://gitlab.com/researchable/general/docker-images/pandoc/commit/be8704df2c7aa23b2a7ea9498bd8ce6411c18695))
